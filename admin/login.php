<?php
    include_once "../libs/include.php";
    is_logged_login();

    $result = login($_POST);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width">
    <link rel="stylesheet" type="text/css" href="../css/login.css">
    <title>Přihlášení</title>
</head>
<body>
<div id="div_container">
    <div id="div_login_form">
        <h1>Fotogalerie</h1>
        <p class="error">
        <?php
            login_error($result);
        ?>
        </p>
        <form action="#" method="post">
            <fieldset>
                <input id="email" type="email" name="email" required>
                <label for="email">E-Mail</label>
                <div class="after"></div>
            </fieldset>
            <fieldset>
                <input id="password" type="password" name="password" required>
                <label for="password">Heslo</label>
                <div class="after"></div>
            </fieldset>
            <fieldset>
                <input id="input_submit" type="submit" value="Přihlásit se">
            </fieldset>
        </form>
    </div>
</div>
</body>
</html>