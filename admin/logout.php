<?php
    include_once "../libs/include.php";

    $user = new User();
    $user->logout();
    header("Location: login.php");