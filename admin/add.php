<?php
    include_once "../libs/include.php";
    is_logged();
    create_album(files($_FILES), $_POST);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Přidat album</title>
    <link rel="stylesheet" type="text/css" href="css/add.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
</head>
<body>
    <div class="header">
        <h1>Fotogalerie</h1>
        <a href="logout.php" title="Odhlásit se" class="logoutLink">
            <div class="logout">
                Odhlásit se
            </div>
        </a>
    </div>
    <div class="main">
        <form action="" method="post" enctype="multipart/form-data">
            <label for="name" title="Název alba" class="albumName">Název alba</label>
            <input type="text" name="name" id="name" >
            <input type="file" name="file[]" multiple class="upload">
            <input type="submit" name="submit" value="Nahrát">
        </form>
    </div>
</body>
</html>