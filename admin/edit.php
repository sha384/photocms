<?php
    include_once "../libs/include.php";
    is_logged();
    edit_album(files($_FILES), $_POST);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Upravit album</title>
    <link rel="stylesheet" type="text/css" href="css/edit.css">
</head>
<body>
    <div class="header">
        <h1>Fotogalerie</h1>
        <a href="logout.php" title="Odhlásit se" class="logoutLink">
            <div class="logout">
                Odhlásit se
            </div>
        </a>
    </div>
    <?php
        $gallery = new Gallery();
        echo $gallery->editAlbum($_GET);
    ?>
</body>
</html>