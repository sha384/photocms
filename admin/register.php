<?php
	include_once "../libs/include.php";
	is_logged_login();

	$result = register($_POST);
	?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width">
    <link rel="stylesheet" type="text/css" href="../css/register.css">
    <title>Registrace</title>
    <link type="text/css">
</head>
<body>
<div id="div_container">
    <div id="div_register_form">
        <h1>Fotogalerie</h1>
		<p class="error">
			<?php
			register_error($result);
			?>
		</p>
        <form action="#" method="post">
            <fieldset>
                <input id="firstname" type="text" name="firstname" required>
                <label for="firstname">Jméno</label>
                <div class="after"></div>
            </fieldset>
            <fieldset>
                <input id="surname" type="text" name="surname" required>
                <label for="surname">Přijímení</label>
                <div class="after"></div>
            </fieldset>
            <fieldset>
                <input id="email" type="email" name="email" required>
                <label for="email">E-Mail</label>
                <div class="after"></div>
            </fieldset>
            <fieldset>
                <input id="password" type="password" name="password" required>
                <label for="password">Heslo</label>
                <div class="after"></div>
            </fieldset>
            <fieldset>
                <input id="password_again" type="password" name="password_again" required>
                <label for="password_again">Heslo znovu</label>
                <div class="after"></div>
            </fieldset>
            <fieldset>
                <input id="input_submit" type="submit" value="Registrovat se">
            </fieldset>
        </form>
    </div>
</div>
</body>
</html>