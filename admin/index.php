<?php
	include_once "../libs/include.php";
	is_logged();

?>
<!DOCTYPE html>
<html lang="cs">
<head>
	<meta charset="UTF-8">
	<title>Fotogalerie</title>
    <link rel="stylesheet" type="text/css" href="css/index.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
</head>
<body>
    <div class="header">
        <h1>Fotogalerie</h1>
        <a href="logout.php" title="Odhlásit se" class="logoutLink">
            <div class="logout">
                Odhlásit se
            </div>
        </a>
    </div>
    <?php
        $gallery = new Gallery();
        echo $gallery->showAlbumsAdmin();
    ?>
    <a href="add.php" title="Přidat album">
        <div class="addAlbum">
            <i class="fas fa-plus"></i>
        </div>
    </a>
</body>
</html>