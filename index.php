<?php
include_once "libs/include.php";
is_logged();

?>
<!DOCTYPE html>
<html lang="cs">
<head>
    <meta charset="UTF-8">
    <title>Fotogalerie</title>
    <link rel="stylesheet" type="text/css" href="css/index.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
</head>
<body>
<!--<form action="#" method="post" enctype="multipart/form-data">
    <input type="file" name="file[]" multiple>
    <input type="submit" name="submit">
</form>-->
<div class="header">
    <h1>Fotogalerie</h1>
</div>
<!--<div class="row">
    <div class="column">
        <div class="editButton">
            <a href="edit.php" title="Upravit" class="editLink">
                <div class="edit">
                    <i class="fas fa-edit"></i>
                </div>
            </a>
        </div>
        <img src="../img/thumbnails/eb0dDGnwfR4.jpg" class="hover-shadow cursor main-thumbnail">
        <p class="albumName">Název alba</p>
    </div>
    <div class="column">
        <img src="../img/thumbnails/6bWPpCdTAU8.jpg" class="hover-shadow cursor main-thumbnail">
        <p class="albumName">Název alba</p>
    </div>
    <div class="column">
        <img src="../img/thumbnails/de61YZOHBs4.jpg" class="hover-shadow cursor main-thumbnail">
        <p class="albumName">Název alba</p>
    </div>
    <div class="column">
        <img src="../img/thumbnails/9Q0g5gJDuBk.jpg" class="hover-shadow cursor main-thumbnail">
        <p class="albumName">Název alba</p>
    </div>
</div>-->
<?php
$gallery = new Gallery();
echo $gallery->showAlbums();
?>
</body>
</html>