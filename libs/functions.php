<?php
    include_once "include.php";

    function redirect($url){
    	if (strpos($url, "/") === 0){
    		$header = "Location: " . path . $url;
    		header($header);
    		exit();
		} else{
    		$header = "Location: " . $url;
    		header($header);
    		exit();
		}
	}
    function escape_sql(&$string){
        $string = htmlspecialchars(strip_tags($string));
        return 1;
    }
    function validate_email($email){
        return filter_var($email, FILTER_VALIDATE_EMAIL);
    }
    function login($post = null){
        if (!empty($post) && isset($post)){
            $user = new User();
            $login = $user->login($post["email"], $post["password"]);
            if ($login){
                redirect("/admin/login.php");
                return true;
            } else{
                return "Nesprávný e-mail nebo heslo!";
            }
        } else {
			return false;
		}
    }
    function register($post = null){
		if (!empty($post) && isset($post)){
			$user = new User();
			$register = $user->register($post["email"], $post["password"], $post["password_again"], $post["firstname"], $post["surname"]);
			if ($register){
				login($post);
				redirect("/admin/register.php");
				return true;
			} elseif (is_array($register)){
				return $register;
			}
		} else {
			return false;
		}
	}
    function login_error($result = null){
        if (is_string($result)){
            echo $result;
        }
    }
    function register_error($error = null){
    	if (is_array($error)){
    		echo implode(" ", $error);
		}
	}
    function is_logged(){
        if (!isset($_SESSION["email"]) && !isset($_SESSION["name"])){
            header("Location: login.php");
        }
    }
    function is_logged_login(){
        if (isset($_SESSION["email"]) && isset($_SESSION["name"])){
            header("Location: index.php");
        }
    }
    function hash_password($password){
    	$password = $password . salt;
    	return hash("sha256", $password);
	}
	function upload_photo($files, $album_id){
    	$mgr = new PhotoMgr();
		return $mgr->uploadPhoto($files, $album_id);
	}
	function create_album($files, $post){
        $mgr = new PhotoMgr();
        if (!empty($files) && !empty($post)){
            $create_album = $mgr->createAlbum($post["name"]);
            if (is_numeric($create_album)){
                if(upload_photo($files, $create_album)){
                    redirect("/admin/index.php");
                }
            }
        } else {
            return false;
        }
    }
    function edit_album($files, $post){
        $mgr = new PhotoMgr();
        if (!empty($files) && !empty($post) && !empty($post["id"])){
            $edit_album = $mgr->editAlbum($post["name"], $post["id"]);
            if(upload_photo($files, $post["id"])){
                redirect("/admin/index.php");
            }
        } else {
            return false;
        }
    }
	function random_string($length = 4) {
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString;
	}
	function escape_spaces($string){
    	return $string = str_replace(' ', '_', $string);
	}
	function files(&$file_post) {
		if (!empty($file_post)){
            $file_ary = array();
            $file_count = count($file_post['file']['name']);
            $file_keys = array_keys($file_post['file']);

            for ($i=0; $i<$file_count; $i++) {
                foreach ($file_keys as $key) {
                    $file_ary[$i][$key] = $file_post['file'][$key][$i];
                }
            }

            return $file_ary;
        }
	}
	function not_found(){
        redirect("/404.html");
    }