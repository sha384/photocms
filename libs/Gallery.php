<?php
include_once "include.php";

class Gallery{
	public function showAlbum($id){
		global $db;
		if (isset($id)){
            $photos = $db->get("SELECT * FROM photos WHERE album_id=:album_id", array(":album_id" => $id));
            $album = $db->get("SELECT * FROM albums WHERE id=:album_id", array(":album_id" => $id));
            $photosCount = count($photos);
            $thumb = "img/thumbnails/";
            $path = "img/";
            $date = new DateTime($album[0]["creation_date"]);
            $creationDate = $date->format('d.m.Y');
            $i = 0;

            $echo = "<div class=\"heading\">";
                $echo .= "<a onclick='window.history.go(-1);' title=\"Zpět\" class='backLink'>";
                    $echo .= "<div class=\"back\">";
                        $echo .= "<i class=\"fas fa-arrow-left\"></i>";
                    $echo .= "</div>";
                $echo .= "</a>";
                $echo .= "<h1>" . $album[0]["name"] . "</h1>";
                $echo .= "<span class=\"creationDate\">";
                    $echo .= "<i class=\"far fa-clock\"></i>";
                    $echo .= "<span> " . $creationDate . "</span>";
                $echo .= "</span>";
            $echo .= "</div>";

            $echo .= "<div class='row'>";
            foreach ($photos as $photo){
                $i++;
                $echo .= "<div class='column'>";
                    $echo .= "<img src=\"" . $thumb . $photo["path"] . "\" onclick=\"openModal();currentSlide(" . $i . ")\" class=\"hover-shadow cursor main-thumbnail\">";
                $echo .= "</div>";
            }
            $echo .= "</div>";

            $i = 0;

            $echo .= "<div id=\"myModal\" class=\"modal\">";
                $echo .= "<span class=\"close cursor\" onclick=\"closeModal()\">&times;</span>";
                $echo .= "<div class='modal-content'>";
                    foreach ($photos as $photo){
                        $i++;
                        $echo .= "<div class='mySlides'>";
                            $echo .= "<div class='numbertext'>" . $i . " / " . $photosCount . "</div>";
                            $echo .= "<img src=\"" . $path . $photo["path"] . "\" class='full-size'>";
                        $echo .= "</div>";
                    }
                    $echo .= "<a class=\"prev\" onclick=\"plusSlides(-1)\">&#10094;</a>";
                    $echo .= "<a class=\"next\" onclick=\"plusSlides(1)\">&#10095;</a>";
                $echo .= "</div>";
            $echo .= "</div>";
            return $echo;
        } else {
		    not_found();
        }
	}
	public function showAlbumsAdmin(){
	    global $db;
	    $albums = $db->get("SELECT * FROM albums");

	    $echo = "<div class='row'>";
	    foreach ($albums as $album){
	        $thumbnail = $db->get("SELECT path FROM photos WHERE album_id=:album_id LIMIT 1", array(":album_id" => $album["id"]));
	        $path = "../img/thumbnails/" . $thumbnail[0]["path"];
	        $echo .= "<div class='column'>";
	            $echo .= "<div class='editButton'>";
	                $echo .= "<a href='edit.php?id=" . $album["id"] . "' title='Upravit' class='editLink'>";
	                    $echo .= "<div class='edit'>";
	                        $echo .= "<i class=\"fas fa-edit\"></i>";
                        $echo .= "</div>";
                    $echo .= "</a>";
                $echo .= "</div>";
            $echo .= "<div class='deleteButton'>";
                $echo .= "<a href='delete.php?id=" . $album["id"] . "' title='Upravit' class='deleteLink'>";
                    $echo .= "<div class='delete'>";
                        $echo .= "<i class=\"fas fa-trash-alt\"></i>";
                    $echo .= "</div>";
                $echo .= "</a>";
            $echo .= "</div>";
                $echo .= "<img src='" . $path . "' class=\"hover-shadow cursor main-thumbnail\">";
                $echo .= "<p class='albumName'>" . $album["name"] . "</p>";
            $echo .= "</div>";
        }
        $echo .= "</div>";
	    return $echo;
    }
    public function showAlbums(){
        global $db;
        $albums = $db->get("SELECT * FROM albums");

        $echo = "<div class='row'>";
        foreach ($albums as $album){
            $thumbnail = $db->get("SELECT path FROM photos WHERE album_id=:album_id LIMIT 1", array(":album_id" => $album["id"]));
            $path = "img/thumbnails/" . $thumbnail[0]["path"];
            $echo .= "<a href='gallery.php?id=" . $album["id"] . "' title='Zobrazit album' class='link'>";
                $echo .= "<div class='column'>";
                    $echo .= "<img src='" . $path . "' class=\"hover-shadow cursor main-thumbnail\">";
                    $echo .= "<p class='albumName'>" . $album["name"] . "</p>";
                $echo .= "</div>";
            $echo .= "</a>";
        }
        $echo .= "</div>";
        return $echo;
    }
    public function editAlbum($get){
        global $db;
        $id = $get["id"];
        if (!empty($id)){
            $album = $db->get("SELECT * FROM albums WHERE id=:id", array(":id" => $id));
            $echo = "<div class='main'>";
                $echo .= "<form action=\"\" method=\"post\" enctype=\"multipart/form-data\">";
                    $echo .= "<label for=\"name\" title=\"Název alba\" class=\"albumName\">Název alba</label>";
                        $echo .= "<input type=\"text\" name=\"name\" id=\"name\" value='" . $album[0]["name"] . "'>";
                    $echo .= "<input type=\"file\" name=\"file[]\" multiple class=\"upload\">";
                    $echo .= "<input name='id' type='hidden' value='" . $album[0]["id"] . "'>";
                    $echo .= "<input type=\"submit\" name=\"submit\" value=\"Nahrát\">";
                $echo .= "</form>";
            $echo .= "</div>";
            return $echo;
        } else {
            not_found();
        }
    }
}