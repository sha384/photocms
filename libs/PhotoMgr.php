<?php
include_once "include.php";

class PhotoMgr{
	public function uploadPhoto($files, $album_id){
		global $db;
		$target_dir = "../img/";
		foreach ($files as $file){
			if ($file["error"] == 1){
				$error[] = "Nepodařilo se nahrát fotku " . $file["name"] . ", zkuste to prosím znovu.";
			}
		}
		foreach ($files as $file){
			$target_file_name = escape_spaces(basename($file["name"]));
			$target_file = $target_dir . $target_file_name;
			$image_file_type = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
			$image_name = pathinfo($target_file, PATHINFO_FILENAME);
			$uploadOk = 1;

			if (file_exists($target_file)) {
				$target_file_name = escape_spaces($image_name) . random_string() . "." .  $image_file_type;
				$target_file = $target_dir . $target_file_name;
			}
			if ($image_file_type != "jpg" && $image_file_type != "png" && $image_file_type != "jpeg") {
				$uploadOk = 0;
				$error[] = "Povolené formáty jsou jpg/jpeg a png.";
			}
			if ($uploadOk == 0) {
				return $error;
			} else {
				if (move_uploaded_file($file["tmp_name"], $target_file)) {
                    $this->makeThumb($target_file_name);
					$data = array(
						":path" => $target_file_name,
						":album_id" => $album_id
					);
					if ($db->write("INSERT INTO photos (path, album_id) VALUES (:path, :album_id)", $data)){

					} else{
						$error[] = "Nezdařil se zápis do databáze, zkuste to prosím znovu.";
					}
				}
			}
		}
		if (!empty($error)){
			return $error;
		} else{
			return true;
		}
	}
	public function deletePhoto($id){
		global $db;
		$photo = $db->get("SELECT * FROM photos WHERE id=:id", array(":id" => $id));
		$filename = "img/" .  $photo[0]["path"];
		if (unlink($filename)){
			$removePhoto = $db->write("DELETE FROM photos WHERE id=:id", array(":id" => $id));
			if ($removePhoto){
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
	public function createAlbum($name){
		global $db;
        if (!empty($db->get("SELECT * FROM albums WHERE name=:name", array(":name" => $name)))){
            return "Album s tímto názvem již existuje!";
        } else{
            $data = array(
                ":name" => $name,
            );
            $result = $db->write("INSERT INTO albums (name) VALUES (:name)", $data);
            if ($result){
                $data = $db->get("SELECT * FROM albums WHERE name=:name", array(":name" => $name));
                return $data[0]["id"];
            } else {
                return false;
            }
        }
	}
    public function editAlbum($name, $id){
        global $db;
        if (!empty($db->get("SELECT * FROM albums WHERE name=:name", array(":name" => $name)))){
            return "Album s tímto názvem již existuje!";
        } else{
            $result = $db->get("UPDATE albums SET name=:name WHERE id=:id", array(":name" => $name, ":id" => $id));
            if ($result){
                return true;
            } else {
                return false;
            }
        }
    }
    public function deleteAlbum($id){
	    global $db;
	    $photos = $db->get("SELECT * FROM photos WHERE album_id=:id", array(":id" =>$id));
	    $path = "../img/";
	    $pathThumb = "../img/thumbnails/";
	    foreach ($photos as $photo){
	        unlink($path . $photo["path"]);
	        unlink($pathThumb . $photo["path"]);
        }
        $delete = $db->write("DELETE FROM photos WHERE album_id=:id", array(":id" =>$id));
	    if ($delete){
            $delete = $db->write("DELETE FROM albums WHERE id=:id", array(":id" =>$id));
            if ($delete){
                return true;
            }
        }
    }
	/* Source: https://stackoverflow.com/questions/11376315/creating-a-thumbnail-from-an-uploaded-image/31880023#31880023 */
	public function makeThumb($src) {
	    $path = "../img/" . $src;
		//$filename = basename($src, PATHINFO_FILENAME);
		$extension = strtolower(pathinfo($src, PATHINFO_EXTENSION));
		$dest = "../img/thumbnails/" . $src;

		if ($extension == "jpg" || $extension == "jpeg"){
			$source_image = imagecreatefromjpeg($path);
		} else{
			$source_image = imagecreatefrompng($path);
		}

		$width = imagesx($source_image);
		$height = imagesy($source_image);
		$desired_height = "180";

		$desired_width = floor($width * ($desired_height / $height));

		$virtual_image = imagecreatetruecolor($desired_width, $desired_height);

		imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);

		if ($extension == "jpg" || $extension == "jpeg"){
			imagejpeg($virtual_image, $dest);
		} else {
			imagepng($virtual_image, $dest);
		}
	}
}