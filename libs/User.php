<?php

include_once "include.php";

class User{
	public function login($email, $password){
        global $db;

        $password = hash_password($password);

	    $result = $db->get("SELECT * FROM users WHERE email=:email AND password=:password", array(":email" => $email, ":password" => $password));
	    if (!empty($result)){
            $_SESSION["email"] = $result[0]["email"];
            $_SESSION["name"] = $result[0]["firstname"] . " " . $result[0]["surname"];
            return true;
        } else{
	        return false;
        }
	}
	public function register($email, $password, $passwordAgain, $firstname, $surname){
		global $db;
		$error = array();
		if (!$this->checkDuplicateAccount($email)){
			$error[] = "Účet s tímto e-mailem již existuje!";
		}
		if (mb_strlen($password) >= 6){
			$password = hash_password($password);
			$passwordAgain = hash_password($passwordAgain);
		} else {
			$error[] = "Heslo musí být delší než 6 znaků!";
		}
		if ($password != $passwordAgain){
			$error[] = "Hesla se neshodují!";
		}
		if (!validate_email($email)){
			$error[] = "E-mail není ve správném formátu!";
		}
		if (count($error) == 0){
			$data = array(
				":firstname" => $firstname,
				":surname" => $surname,
				":password" => $password,
				":email" => $email
			);
			$result = $db->write("INSERT INTO users (firstname, surname, email, password) VALUES (:firstname, :surname, :email, :password)", $data);
			return true;
		} else{
			return $error;
		}
	}
	private function checkDuplicateAccount($email){
		global $db;

		$result = $db->get("SELECT * FROM users WHERE email=:email", array(":email" => $email));
		if (!empty($result)){
			return false;
		} else {
			return true;
		}
	}
	public function logout(){
	    session_destroy();
	    return true;
    }
}