<?php
    session_start();

    define("salt", "eae");
    define("path", "/photocms");

    include_once "Db.php";
    include_once "User.php";
    include_once "functions.php";
    include_once "PhotoMgr.php";
    include_once "Gallery.php";

    global $db;
    $db = new Db();