<?php
define("dbserver", "localhost");
define("dbuser", "root");
define("dbpass", "");
define("dbname", "photocms");

include_once "include.php";


class Db{
    private static $db;
    private static $settings = array(
        PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
        PDO::MYSQL_ATTR_INIT_COMMAND => "SET CHARACTER SET utf8"
    );
    public function __construct(){
        self::$db = new PDO("mysql:host=" .dbserver. ";dbname=" .dbname,dbuser,dbpass, self::$settings);
    }
    function write($query, $data = false){
        $db = self::$db;

        $prepare = $db->prepare($query);
        $data ? $prepare->execute($data) : $prepare->execute();
        $dbError = $prepare->errorInfo();
        if ($dbError[1] != null){
            return $dbError;
        } else {
            return true;
        }
    }
    function get($query, $data = array()){
        $db = self::$db;

        $prepare = $db->prepare($query);
        if (!empty($data)){
            $prepare->execute($data);
        } else {
            $prepare->execute();
        }
        $dbError = $prepare->errorInfo();
        if ($dbError[1] != null){
            return $dbError;
        } else {
            return $prepare->fetchAll(PDO::FETCH_ASSOC);
        }
    }
}